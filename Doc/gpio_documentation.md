## 引脚说明

### LED

 LED|PIN
 -|-
 LED1|GPIOD12

---

### 按键

1. **4x4矩阵**

    Rows and Columns|PIN  
    -|-
    R1|GPIOI11
    R2|GPIOC11
    R3|GPIOC12
    R4|GPIOC9
    C1|GPIOC8
    C2|GPIOD2
    C3|GPIOC10
    C4|GPIOA5
    
    - 矩阵键盘采用扫描模式，默认Rn引脚为输出模式，Cn引脚为输入模式。扫描时Rn输出高电平。
    
2. **独立按键**
    
    KEY|PIN  
    -|-
    K1|GPIOA0

---

### USART

USART|PIN  
-|-
USART1_RX|GPIOA10
USART1_TX|GPIOA9
USART2_RX|GPIOD6
USART2_TX|GPIOD5
USART3_RX|GPIOB11
USART3_TX|GPIOB10

- USART1默认作为 `RT-Thread` 的 `shell` 窗口

