/**
  ******************************************************************************
  * @file    main.c
  * @author  chendu
  * @version V1.0
  * @date    2020-xx-xx
  * @brief   rtt base
  ******************************************************************************
  * @attention
  *
  * 实验平台:野火  STM32 F429 开发板 
  * 论坛    :http://www.firebbs.cn
  * 淘宝    :https://fire-stm32.taobao.com
  *
  ******************************************************************************
  */
#include "stm32f4xx.h"
#include "rtthread.h"
#include "bsp_led.h"
#include "bsp_uart.h"
#include "bsp_key4x4.h"
#include "bsp_key.h"


extern usart_dev_t usart_dev1;

void key_callback(void *data);

key_t k2 = KEY(A, 0, key_callback, "k2 callback\n");


void key_callback(void *data) {
    rt_kprintf("%s", data);
}

int usr_init() {
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    
    led_init();
    usart_init(&usart_dev1);
    key4x4_init();
    
    key_init(&k2);
    
    return RT_EOK;
}
INIT_BOARD_EXPORT(usr_init);

int main(void) {
    key4x4_set_callback(KEY_S1, key_callback, "s1 callback\n");
    key4x4_set_callback(KEY_S2, key_callback, "s2 callback\n");
    key4x4_set_callback(KEY_S3, key_callback, "s3 callback\n");
    key4x4_set_callback(KEY_S4, key_callback, "s4 callback\n");
    key4x4_set_callback(KEY_S5, key_callback, "s5 callback\n");
    key4x4_set_callback(KEY_S6, key_callback, "s6 callback\n");
    key4x4_set_callback(KEY_S7, key_callback, "s7 callback\n");
    key4x4_set_callback(KEY_S8, key_callback, "s8 callback\n");
    key4x4_set_callback(KEY_S9, key_callback, "s9 callback\n");
    key4x4_set_callback(KEY_S10, key_callback, "s10 callback\n");
    key4x4_set_callback(KEY_S11, key_callback, "s11 callback\n");
    key4x4_set_callback(KEY_S12, key_callback, "s12 callback\n");
    key4x4_set_callback(KEY_S13, key_callback, "s13 callback\n");
    key4x4_set_callback(KEY_S14, key_callback, "s14 callback\n");
    key4x4_set_callback(KEY_S15, key_callback, "s15 callback\n");
    key4x4_set_callback(KEY_S16, key_callback, "s16 callback\n");
}


/*********************************************END OF FILE**********************/

