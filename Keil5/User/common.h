#ifndef __COMMON_H
#define __COMMON_H 			   
#include "stm32f4xx.h"
#include "rtthread.h"


#define EXTI5_IRQn      -1
#define EXTI6_IRQn      -1
#define EXTI7_IRQn      -1
#define EXTI8_IRQn      -1
#define EXTI9_IRQn      -1
#define EXTI10_IRQn     -1
#define EXTI11_IRQn     -1
#define EXTI12_IRQn     -1
#define EXTI13_IRQn     -1
#define EXTI14_IRQn     -1
#define EXTI15_IRQn     -1

#define GPIO_PIN(port, pin)         {GPIO##port, RCC_AHB1Periph_GPIO##port, GPIO_Pin_##pin}

#define GPIO_EXTI_IRQ(pin)          (pin < 5 ? EXTI##pin##_IRQn : (pin > 9 ? EXTI15_10_IRQn : EXTI9_5_IRQn))
#define GPIO_EXTI(port, pin)        {EXTI_PortSourceGPIO##port, EXTI_PinSource##pin, EXTI_Line##pin, GPIO_EXTI_IRQ(pin)}

#define GPIO_PIN_EXTI(port, pin)    {GPIO_PIN(port, pin), GPIO_EXTI(port, pin)}

typedef void (*callback_func_t)(void *);
typedef void *  callback_data_t;

typedef struct {
    GPIO_TypeDef   *gpio_port;  // GPIOA
    uint32_t        gpio_clk;   // RCC_AHB1Periph_GPIOA
    uint16_t        gpio_pin;   // GPIO_Pin_0
} gpio_pin_t;

typedef struct {
    uint8_t     exti_port;      // EXTI_PortSourceGPIOA
    uint8_t     exti_pin;       // EXTI_PinSource0
    uint32_t    exti_line;      // EXTI_Line0
    uint8_t     exti_irq;       // EXTI0_IRQ0
} gpio_exti_t;

typedef struct {
	u16 data0:1;
	u16 data1:1;
	u16 data2:1;
	u16 data3:1;
	u16 data4:1;
	u16 data5:1;
	u16 data6:1;
	u16 data7:1;
	u16 data8:1;
	u16 data9:1;
	u16 data10:1;
	u16 data11:1;
	u16 data12:1;	
	u16 data13:1;
	u16 data14:1;
	u16 data15:1;	
} _gpio_group;

//位带操作,实现51类似的GPIO控制功能
//具体实现思想,参考<<CM3权威指南>>第五章(87页~92页).M4同M3类似,只是寄存器地址变了.
//IO口操作宏定义
#define BITBAND(addr, bitnum) ((addr & 0xF0000000)+0x2000000+((addr &0xFFFFF)<<5)+(bitnum<<2)) 
#define MEM_ADDR(addr)  *((volatile unsigned long  *)(addr)) 
#define BIT_ADDR(addr, bitnum)   MEM_ADDR(BITBAND(addr, bitnum)) 
//IO口地址映射
#define GPIOA_ODR_Addr    (GPIOA_BASE+20) //0x40020014
#define GPIOB_ODR_Addr    (GPIOB_BASE+20) //0x40020414 
#define GPIOC_ODR_Addr    (GPIOC_BASE+20) //0x40020814 
#define GPIOD_ODR_Addr    (GPIOD_BASE+20) //0x40020C14 
#define GPIOE_ODR_Addr    (GPIOE_BASE+20) //0x40021014 
#define GPIOF_ODR_Addr    (GPIOF_BASE+20) //0x40021414    
#define GPIOG_ODR_Addr    (GPIOG_BASE+20) //0x40021814   
#define GPIOH_ODR_Addr    (GPIOH_BASE+20) //0x40021C14    
#define GPIOI_ODR_Addr    (GPIOI_BASE+20) //0x40022014     

#define GPIOA_IDR_Addr    (GPIOA_BASE+16) //0x40020010 
#define GPIOB_IDR_Addr    (GPIOB_BASE+16) //0x40020410 
#define GPIOC_IDR_Addr    (GPIOC_BASE+16) //0x40020810 
#define GPIOD_IDR_Addr    (GPIOD_BASE+16) //0x40020C10 
#define GPIOE_IDR_Addr    (GPIOE_BASE+16) //0x40021010 
#define GPIOF_IDR_Addr    (GPIOF_BASE+16) //0x40021410 
#define GPIOG_IDR_Addr    (GPIOG_BASE+16) //0x40021810 
#define GPIOH_IDR_Addr    (GPIOH_BASE+16) //0x40021C10 
#define GPIOI_IDR_Addr    (GPIOI_BASE+16) //0x40022010 
 
//IO口操作,只对单一的IO口!
//确保n的值小于16!
#define PAout(n)   BIT_ADDR(GPIOA_ODR_Addr,n)  //输出 
#define PAin(n)    BIT_ADDR(GPIOA_IDR_Addr,n)  //输入 

#define PBout(n)   BIT_ADDR(GPIOB_ODR_Addr,n)  //输出 
#define PBin(n)    BIT_ADDR(GPIOB_IDR_Addr,n)  //输入 

#define PCout(n)   BIT_ADDR(GPIOC_ODR_Addr,n)  //输出 
#define PCin(n)    BIT_ADDR(GPIOC_IDR_Addr,n)  //输入 

#define PDout(n)   BIT_ADDR(GPIOD_ODR_Addr,n)  //输出 
#define PDin(n)    BIT_ADDR(GPIOD_IDR_Addr,n)  //输入 

#define PEout(n)   BIT_ADDR(GPIOE_ODR_Addr,n)  //输出 
#define PEin(n)    BIT_ADDR(GPIOE_IDR_Addr,n)  //输入

#define PFout(n)   BIT_ADDR(GPIOF_ODR_Addr,n)  //输出 
#define PFin(n)    BIT_ADDR(GPIOF_IDR_Addr,n)  //输入

#define PGout(n)   BIT_ADDR(GPIOG_ODR_Addr,n)  //输出 
#define PGin(n)    BIT_ADDR(GPIOG_IDR_Addr,n)  //输入

#define PHout(n)   BIT_ADDR(GPIOH_ODR_Addr,n)  //输出 
#define PHin(n)    BIT_ADDR(GPIOH_IDR_Addr,n)  //输入

#define PIout(n)   BIT_ADDR(GPIOI_ODR_Addr,n)  //输出 
#define PIin(n)    BIT_ADDR(GPIOI_IDR_Addr,n)  //输入

#define SYSCLK 168    //系统时钟

#endif





























