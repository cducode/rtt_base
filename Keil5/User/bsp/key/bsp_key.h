#ifndef __KEY_H
#define __KEY_H
#include "common.h"
#include "stm32f4xx.h" 

/************************************************************/

#define KEY_TOTAL   4

#define KEY(port, pin, func, data)         {GPIO_PIN(port, pin), GPIO_EXTI(port, pin), func, data}

typedef struct {
    gpio_pin_t pin;
    gpio_exti_t exti;
    callback_func_t func;
    callback_data_t data;
} key_t;

void key_init(key_t *key);

static int get_index(uint32_t line);

#endif
