#include "bsp_key4x4.h"
#include "rtthread.h"


struct rt_thread keyscan_thread;
struct rt_thread key_callback_thread;

rt_uint8_t keyscan_thread_stack[512];
rt_uint8_t key_callback_thread_stack[512];

struct rt_semaphore cb_sem;
struct rt_mutex scan_mut;
struct rt_timer key_check_timer;

/* 引脚定义 */
key_pin_t key_c1 = GPIO_PIN_EXTI(C, 8);
key_pin_t key_c2 = GPIO_PIN_EXTI(D, 2);
key_pin_t key_c3 = GPIO_PIN_EXTI(C, 10);
key_pin_t key_c4 = GPIO_PIN_EXTI(A, 5);
key_pin_t key_r1 = GPIO_PIN(I, 11);
key_pin_t key_r2 = GPIO_PIN(C, 11);
key_pin_t key_r3 = GPIO_PIN(C, 12);
key_pin_t key_r4 = GPIO_PIN(C, 9);

key_pin_t *key_col[4] = {&key_c1, &key_c2, &key_c3, &key_c4};
key_pin_t *key_row[4] = {&key_r1, &key_r2, &key_r3, &key_r4};

u32 cur_row = 0;

void key4x4_init(void) {
    GPIO_InitTypeDef  GPIO_InitStructure;
    EXTI_InitTypeDef EXTI_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    int i;
    
    for (i = 0; i < 4; i++) {
        RCC_AHB1PeriphClockCmd(key_col[i]->pin.gpio_clk | key_row[i]->pin.gpio_clk, ENABLE);     // 时钟使能
    }
    
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;               // 普通输出模式
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;              // 推挽输出
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;           // 50MHz
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;              // 下拉
    
    for (i = 0; i < 4; i++) {
        GPIO_InitStructure.GPIO_Pin = key_row[i]->pin.gpio_pin;
        GPIO_Init(key_row[i]->pin.gpio_port, &GPIO_InitStructure);      // 初始化GPIO
        GPIO_ResetBits(key_row[i]->pin.gpio_port, key_row[i]->pin.gpio_pin);
    }
    
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);      // 使能 SYSCFG 时钟，使用GPIO外部中断时必须使能SYSCFG时钟
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;                // 普通输入模式
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;           // 50M
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;              // 下拉
    
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;         // 中断模式
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;     // 下降沿触发
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;                   // 使能
    
    
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;   // 配置中断优先级
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    
    for (i = 0; i < 4; i++) {
        GPIO_InitStructure.GPIO_Pin = key_col[i]->pin.gpio_pin;
        GPIO_Init(key_col[i]->pin.gpio_port, &GPIO_InitStructure);
      
        NVIC_InitStructure.NVIC_IRQChannel = key_col[i]->exti.exti_irq;
        NVIC_Init(&NVIC_InitStructure);
        
        SYSCFG_EXTILineConfig(key_col[i]->exti.exti_port, key_col[i]->exti.exti_pin);   // 连接中断源到引脚
        EXTI_InitStructure.EXTI_Line = key_col[i]->exti.exti_line;      // 设置中断源
        EXTI_Init(&EXTI_InitStructure);
    }
}

void key_check_timer_timeout(void *parameter) {
    rt_kprintf("key_check_timer_timeout\n");
    
//    rt_mutex_take(&scan_mut, RT_WAITING_FOREVER);
    
    if(GPIO_ReadOutputDataBit(key_col[1]->pin.gpio_port, key_col[1]->pin.gpio_pin) == 1) {
        rt_timer_start(&key_check_timer);
        return;
    }
    
    rt_kprintf("key_check is available\n");
    
    EXTI->IMR |= key_col[1]->exti.exti_line;  // 开启外部中断响应
//    rt_mutex_release(&scan_mut);
}

void key4x4_callback_thread_entry(void *parameter) {
    while (1) {
        rt_sem_take(&cb_sem, RT_WAITING_FOREVER);
        
        rt_timer_start(&key_check_timer);
        
//        rt_kprintf("EXTI c2 callback start\n");
//        rt_thread_delay(1000);
//        rt_kprintf("EXTI c2 callback end\n");
    }
}

void key4x4_scan_thread_entry(void *parameter) {
    uint32_t i = 0;
    
    rt_mutex_take(&scan_mut, RT_WAITING_FOREVER);
    
    while (1) {
        cur_row = i%4;
        
//        rt_mutex_release(&scan_mut);
        
        GPIO_SetBits(key_row[cur_row]->pin.gpio_port, key_row[cur_row]->pin.gpio_pin);  // 行依次设置为高电平
        rt_thread_delay(20);
        GPIO_ResetBits(key_row[cur_row]->pin.gpio_port, key_row[cur_row]->pin.gpio_pin);
        
//        rt_mutex_take(&scan_mut, RT_WAITING_FOREVER);
        
        i++;
    }
}

int key_thread_init(void) {
    rt_err_t ret;
    
    rt_sem_init(&cb_sem, "cb_sem", 0, RT_IPC_FLAG_FIFO);
    rt_mutex_init(&scan_mut, "scan_mut", RT_IPC_FLAG_FIFO);
    
    rt_timer_init(&key_check_timer, "key_check", key_check_timer_timeout, NULL, 50, RT_TIMER_FLAG_ONE_SHOT);
    
    rt_thread_init(&keyscan_thread, "key_scan", key4x4_scan_thread_entry, RT_NULL, &keyscan_thread_stack, sizeof(keyscan_thread_stack), 2, 10);
    ret = rt_thread_startup(&keyscan_thread);
    if (ret != RT_EOK) {
        rt_kprintf("key_scan thread startup ERROR\n");
        return ret;
    }
    
    rt_thread_init(&key_callback_thread, "key_cb", key4x4_callback_thread_entry, RT_NULL, &key_callback_thread_stack, sizeof(key_callback_thread_stack), 2, 10);
    ret = rt_thread_startup(&key_callback_thread);
    if (ret != RT_EOK) {
        rt_kprintf("key_callback thread startup ERROR\n");
        return ret;
    }
    
    return RT_EOK;
}
INIT_APP_EXPORT(key_thread_init);

void EXTI2_IRQHandler(void) {
    rt_interrupt_enter();
    
	if(EXTI_GetITStatus(key_c2.exti.exti_line) != RESET) {
        EXTI->IMR &= ~(key_c2.exti.exti_line);  // 关闭外部中断响应
        
        rt_sem_release(&cb_sem);
        rt_kprintf("EXTI c2 row[%d]\n", cur_row);
		EXTI_ClearITPendingBit(key_c2.exti.exti_line);
	}
    
    rt_interrupt_leave();
}

void EXTI9_5_IRQHandler(void) {
    rt_interrupt_enter();
    
    if(EXTI_GetITStatus(key_c1.exti.exti_line) != RESET) {
        rt_kprintf("EXTI c1 row[%d]\n", cur_row);
		EXTI_ClearITPendingBit(key_c1.exti.exti_line);
	}
    
    if(EXTI_GetITStatus(key_c4.exti.exti_line) != RESET) {
        rt_kprintf("EXTI c4 row[%d]\n", cur_row);
		EXTI_ClearITPendingBit(key_c4.exti.exti_line);
	}
    
    rt_interrupt_leave();
}

void EXTI15_10_IRQHandler(void) {
    rt_interrupt_enter();
    
	if(EXTI_GetITStatus(key_c3.exti.exti_line) != RESET) {
        rt_kprintf("EXTI c3 row[%d]\n", cur_row);
		EXTI_ClearITPendingBit(key_c3.exti.exti_line);
	}
    
    rt_interrupt_leave();
}
