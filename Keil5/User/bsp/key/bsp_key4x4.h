#ifndef __KEY4x4_H
#define __KEY4x4_H
#include "common.h"
#include "stm32f4xx.h" 

/************************************************************/

#define KEY4X4_TOTAL   4*4

enum {
    KEY_S1 = 0,
    KEY_S2, KEY_S3, KEY_S4, KEY_S5, KEY_S6, KEY_S7, KEY_S8, KEY_S9, KEY_S10, KEY_S11, KEY_S12, KEY_S13, KEY_S14, KEY_S15, KEY_S16
};

void key4x4_init(void);
void key4x4_scan_thread_entry(void *parameter);

void key4x4_set_callback(int key, callback_func_t func, void *data);

#endif
