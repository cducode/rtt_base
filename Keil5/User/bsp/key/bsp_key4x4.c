#include "bsp_key4x4.h"
#include "rtthread.h"


struct rt_thread key4x4_scan_thread;
struct rt_thread key4x4_callback_thread;

rt_uint8_t key4x4_scan_thread_stack[512];
rt_uint8_t key4x4_callback_thread_stack[512];

static struct rt_semaphore cb_sem;

/* 引脚定义 */
gpio_pin_t key4x4_c1 = GPIO_PIN(C, 8);
gpio_pin_t key4x4_c2 = GPIO_PIN(D, 2);
gpio_pin_t key4x4_c3 = GPIO_PIN(C, 10);
gpio_pin_t key4x4_c4 = GPIO_PIN(A, 5);
gpio_pin_t key4x4_r1 = GPIO_PIN(I, 11);
gpio_pin_t key4x4_r2 = GPIO_PIN(C, 11);
gpio_pin_t key4x4_r3 = GPIO_PIN(C, 12);
gpio_pin_t key4x4_r4 = GPIO_PIN(C, 9);

gpio_pin_t *key4x4_col[4] = {&key4x4_c1, &key4x4_c2, &key4x4_c3, &key4x4_c4};
gpio_pin_t *key4x4_row[4] = {&key4x4_r1, &key4x4_r2, &key4x4_r3, &key4x4_r4};

u32 press_row = 0;
u32 press_col = 0;

static callback_func_t cb_func[KEY4X4_TOTAL] = {0};
static callback_data_t cb_data[KEY4X4_TOTAL] = {0};


void key4x4_init(void) {
    GPIO_InitTypeDef  GPIO_InitStructure;
    int i;
    
    for (i = 0; i < 4; i++) {
        RCC_AHB1PeriphClockCmd(key4x4_col[i]->gpio_clk | key4x4_row[i]->gpio_clk, ENABLE);     // 时钟使能
    }
    
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;               // 普通输出模式
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;              // 推挽输出
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;           // 50MHz
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;              // 下拉
    
    for (i = 0; i < 4; i++) {
        GPIO_InitStructure.GPIO_Pin = key4x4_row[i]->gpio_pin;
        GPIO_Init(key4x4_row[i]->gpio_port, &GPIO_InitStructure);      // 初始化GPIO
        GPIO_ResetBits(key4x4_row[i]->gpio_port, key4x4_row[i]->gpio_pin);
    }
    
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;                // 普通输入模式
    for (i = 0; i < 4; i++) {
        GPIO_InitStructure.GPIO_Pin = key4x4_col[i]->gpio_pin;
        GPIO_Init(key4x4_col[i]->gpio_port, &GPIO_InitStructure);
    }
}

void key4x4_callback_thread_entry(void *parameter) {
    int key_num = 0;
    while (1) {
        rt_sem_take(&cb_sem, RT_WAITING_FOREVER);
        
        key_num = press_row * 4 + press_col;
        
        if (cb_func[key_num] != NULL) {
            cb_func[key_num](cb_data[key_num]);
        }
    }
}

void key4x4_scan_thread_entry(void *parameter) {
    uint32_t i = 0, c;
    
    while (1) {
        GPIO_SetBits(key4x4_row[i % 4]->gpio_port, key4x4_row[i % 4]->gpio_pin);  // 行依次设置为高电平
        rt_thread_delay(10);
        
        for (c = 0; c < 4; c++) {
            if(GPIO_ReadInputDataBit(key4x4_col[c]->gpio_port, key4x4_col[c]->gpio_pin) != 1) {
                continue;
            }
            rt_thread_delay(10);
            if(GPIO_ReadInputDataBit(key4x4_col[c]->gpio_port, key4x4_col[c]->gpio_pin) == 1) {
                press_row = i % 4;
                press_col = c;
                
                while (GPIO_ReadInputDataBit(key4x4_col[press_col]->gpio_port, key4x4_col[press_col]->gpio_pin) != 0) {
                    rt_thread_delay(10);
                }
                
                rt_sem_release(&cb_sem);
            }
        }
        
        GPIO_ResetBits(key4x4_row[i % 4]->gpio_port, key4x4_row[i % 4]->gpio_pin);
        i++;
    }
}

int key4x4_thread_init(void) {
    rt_err_t ret;
    
    rt_sem_init(&cb_sem, "cb_sem", 0, RT_IPC_FLAG_FIFO);
    
    rt_thread_init(&key4x4_scan_thread, "key_scan", key4x4_scan_thread_entry, RT_NULL, &key4x4_scan_thread_stack, sizeof(key4x4_scan_thread_stack), 2, 10);
    ret = rt_thread_startup(&key4x4_scan_thread);
    if (ret != RT_EOK) {
        rt_kprintf("key_scan thread startup ERROR\n");
        return ret;
    }
    
    rt_thread_init(&key4x4_callback_thread, "key_cb", key4x4_callback_thread_entry, RT_NULL, &key4x4_callback_thread_stack, sizeof(key4x4_callback_thread_stack), 2, 10);
    ret = rt_thread_startup(&key4x4_callback_thread);
    if (ret != RT_EOK) {
        rt_kprintf("key4x4_callback thread startup ERROR\n");
        return ret;
    }
    
    return RT_EOK;
}
INIT_APP_EXPORT(key4x4_thread_init);

void key4x4_set_callback(int key, callback_func_t func, void *data) {
    if (key < 0 || key >= KEY4X4_TOTAL) {
        rt_kprintf("key[KEY_S1 - KEY_S16], you set[%d] ERROR\n", key);
        return;
    }
    
    cb_func[key] = func;
    cb_data[key] = data;
}
