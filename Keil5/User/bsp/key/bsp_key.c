#include "bsp_key.h"
#include "rtthread.h"


struct rt_thread key_callback_thread;

static rt_uint8_t key_callback_thread_stack[512];

static struct rt_semaphore cb_sem;

static key_t *key_total[KEY_TOTAL];
static int key_count = 0;
static uint32_t cur_line = 0;

static uint8_t exti_line[16] = {0};    // 记录中断线对应的cb_func索引

void key_init(key_t *key) {
    GPIO_InitTypeDef  GPIO_InitStructure;
    EXTI_InitTypeDef EXTI_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    
    if (key_count >= KEY_TOTAL) {
        rt_kprintf("key_total overflow!\n");
        return;
    }
    
    exti_line[get_index(key->exti.exti_line)] = key_count;
    key_total[key_count++] = key;
    
    RCC_AHB1PeriphClockCmd(key->pin.gpio_clk, ENABLE);              // 时钟使能
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);          // 使能 SYSCFG 时钟，使用GPIO外部中断时必须使能SYSCFG时钟
    
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;                    // 普通输出模式
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;               // 50MHz
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;                  // 下拉，针对上升沿中断
    GPIO_InitStructure.GPIO_Pin = key->pin.gpio_pin;
    GPIO_Init(key->pin.gpio_port, &GPIO_InitStructure);             // 初始化GPIO
    
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;       // 配置中断优先级
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_InitStructure.NVIC_IRQChannel = key->exti.exti_irq;
    NVIC_Init(&NVIC_InitStructure);
        
    SYSCFG_EXTILineConfig(key->exti.exti_port, key->exti.exti_pin); // 连接中断源到引脚
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;             // 中断模式
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;         // 下降沿中断
    EXTI_InitStructure.EXTI_Line = key->exti.exti_line;             // 设置中断源
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;                       // 使能
    EXTI_Init(&EXTI_InitStructure);
}

void key_callback_thread_entry(void *parameter) {
    int key_num = 0;
    while (1) {
        rt_sem_take(&cb_sem, RT_WAITING_FOREVER);
        rt_thread_delay(10);        // 消抖
        
        EXTI->IMR |= *(uint32_t *)parameter;        // 恢复中断
        key_num = exti_line[get_index(*(uint32_t *)parameter)];
        
        if (key_total[key_num] != NULL) {
            key_total[key_num]->func(key_total[key_num]->data);     // 回调
        }
    }
}

int key_thread_init(void) {
    rt_err_t ret;
    
    rt_sem_init(&cb_sem, "cb_sem", 0, RT_IPC_FLAG_FIFO);
    
    rt_thread_init(&key_callback_thread, "key_cb", key_callback_thread_entry, &cur_line, &key_callback_thread_stack, sizeof(key_callback_thread_stack), 2, 10);
    ret = rt_thread_startup(&key_callback_thread);
    if (ret != RT_EOK) {
        rt_kprintf("key_callback thread startup ERROR\n");
        return ret;
    }
    
    return RT_EOK;
}
INIT_APP_EXPORT(key_thread_init);

void EXTI0_IRQHandler(void) {
    rt_interrupt_enter();
    
	if(EXTI_GetITStatus(EXTI_Line0) != RESET) {
        cur_line = EXTI_Line0;
        EXTI->IMR &= ~(EXTI_Line0);  // 关闭外部中断响应，消抖
        
        rt_sem_release(&cb_sem);
		EXTI_ClearITPendingBit(EXTI_Line0);
	}
    
    rt_interrupt_leave();
}

int get_index(uint32_t line) {
    switch (line) {
        case EXTI_Line0:
            return 0;
        case EXTI_Line1:
            return 1;
        case EXTI_Line2:
            return 2;
        case EXTI_Line3:
            return 3;
        case EXTI_Line4:
            return 4;
        case EXTI_Line5:
            return 5;
        case EXTI_Line6:
            return 6;
        case EXTI_Line7:
            return 7;
        case EXTI_Line8:
            return 8;
        case EXTI_Line9:
            return 9;
        case EXTI_Line10:
            return 10;
        case EXTI_Line11:
            return 11;
        case EXTI_Line12:
            return 12;
        case EXTI_Line13:
            return 13;
        case EXTI_Line14:
            return 14;
        case EXTI_Line15:
            return 15;
        default:
            return 0;
    }
}
