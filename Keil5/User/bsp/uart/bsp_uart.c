#include "bsp_uart.h"
#include "rtthread.h"


#ifdef RT_USING_CONSOLE
struct rt_device rtt_console;
char console_receive_buf[10] = {0};
int console_receive_count = 0;
int console_read_count = 0;
#endif

usart_dev_t usart_dev1 = {
    .name = USART1,
    .clk = RCC_APB2Periph_USART1,
    .baudrate = 115200,
    .rx_pin.gpio_port = GPIOA,
    .rx_pin.gpio_clk = RCC_AHB1Periph_GPIOA,
    .rx_pin.gpio_pin = GPIO_Pin_10,
    .rx_af = GPIO_AF_USART1,
    .rx_source = GPIO_PinSource10,
    .tx_pin.gpio_port = GPIOA,
    .tx_pin.gpio_clk = RCC_AHB1Periph_GPIOA,
    .tx_pin.gpio_pin = GPIO_Pin_9,
    .tx_af = GPIO_AF_USART1,
    .tx_source = GPIO_PinSource9,
    .irq = USART1_IRQn,
};
usart_dev_t usart_dev2 = {
    .name = USART2,
    .clk = RCC_APB1Periph_USART2,
    .baudrate = 115200,
    .rx_pin.gpio_port = GPIOD,
    .rx_pin.gpio_clk = RCC_AHB1Periph_GPIOD,
    .rx_pin.gpio_pin = GPIO_Pin_6,
    .rx_af = GPIO_AF_USART2,
    .rx_source = GPIO_PinSource6,
    .tx_pin.gpio_port = GPIOD,
    .tx_pin.gpio_clk  = RCC_AHB1Periph_GPIOD,
    .tx_pin.gpio_pin = GPIO_Pin_5,
    .tx_af = GPIO_AF_USART2,
    .tx_source = GPIO_PinSource5,
    .irq = USART2_IRQn,
};
usart_dev_t usart_dev3 = {
    .name = USART3,
    .clk = RCC_APB1Periph_USART3,
    .baudrate = 115200,
    .rx_pin.gpio_port = GPIOB,
    .rx_pin.gpio_clk = RCC_AHB1Periph_GPIOB,
    .rx_pin.gpio_pin = GPIO_Pin_11,
    .rx_af = GPIO_AF_USART3,
    .rx_source = GPIO_PinSource11,
    .tx_pin.gpio_port = GPIOB,
    .tx_pin.gpio_clk  = RCC_AHB1Periph_GPIOB,
    .tx_pin.gpio_pin = GPIO_Pin_10,
    .tx_af = GPIO_AF_USART3,
    .tx_source = GPIO_PinSource10,
    .irq = USART3_IRQn,
};

 /**
  * @brief  DEBUG_USART GPIO 配置,工作模式配置。115200 8-N-1 ，中断接收模式
  * @param  无
  * @retval 无
  */
void usart_init(usart_dev_t *usart) {
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
        
    RCC_AHB1PeriphClockCmd(usart->rx_pin.gpio_clk | usart->tx_pin.gpio_clk , ENABLE);

    if (usart->name == USART1 || usart->name == USART6) {
        RCC_APB2PeriphClockCmd(usart->clk, ENABLE);  // 使能 USART 时钟
    } else {
        RCC_APB1PeriphClockCmd(usart->clk, ENABLE);
    }
    
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;  
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;    // 配置复用功能
    GPIO_InitStructure.GPIO_Pin = usart->tx_pin.gpio_pin;  
    GPIO_Init(usart->tx_pin.gpio_port, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Pin = usart->rx_pin.gpio_pin;
    GPIO_Init(usart->rx_pin.gpio_port, &GPIO_InitStructure);

    GPIO_PinAFConfig(usart->tx_pin.gpio_port, usart->tx_source, usart->tx_af);     // 连接复用引脚
    GPIO_PinAFConfig(usart->rx_pin.gpio_port, usart->rx_source, usart->rx_af);

    USART_InitStructure.USART_BaudRate = usart->baudrate;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    USART_Init(usart->name, &USART_InitStructure); 

    
    NVIC_InitStructure.NVIC_IRQChannel = usart->irq;     // 配置中断优先级
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    USART_ITConfig(usart->name, USART_IT_RXNE, ENABLE);
    USART_Cmd(usart->name, ENABLE);
    
#ifdef RT_USING_CONSOLE
    rtt_console.type = RT_Device_Class_Char;
    rtt_console.rx_indicate = NULL;
    rtt_console.tx_complete = NULL;
    
    rtt_console.write = rtt_console_write;
    rtt_console.read = rtt_console_read;
    
    rt_err_t ret = rt_device_register(&rtt_console, "usart1", RT_DEVICE_FLAG_ACTIVATED);
    if(ret != RT_EOK) {
        rt_kprintf("rtt_console register ERROR\n");
    }
#endif
}

#ifdef RT_USING_CONSOLE
rt_size_t rtt_console_write(rt_device_t dev, rt_off_t pos, const void *buffer, rt_size_t size) {
    rt_hw_console_output(buffer);
    
    return size;
}

rt_size_t rtt_console_read(rt_device_t dev, rt_off_t pos, void *buffer, rt_size_t size) {
    rt_size_t ret= 0;
    
    if (console_read_count != console_receive_count) {
        *(char *)buffer = console_receive_buf[console_read_count++];
        console_read_count %= 10;
        ret = 1;
    }
    
    return ret;
}

#endif

void usart_send_char(USART_TypeDef *usartx, uint8_t ch) {
	USART_SendData(usartx, ch);

	while (USART_GetFlagStatus(usartx, USART_FLAG_TXE) == RESET);   // 等待发送数据寄存器为空
}

void usart_send_chars(USART_TypeDef *usartx, uint8_t *str, uint32_t strlen) { 
    uint32_t i = 0;
    
    for (i = 0; i < strlen; i++) {
        usart_send_char(usartx, str[i]);
    }
} 

void usart_send_string(USART_TypeDef *usartx, char *str) {
    unsigned int k=0;
    do {
        usart_send_char(usartx, *(str + k));
        k++;
    } while(*(str + k)!='\0');

    while(USART_GetFlagStatus(usartx, USART_FLAG_TC)==RESET);
}

void rt_hw_console_output(const char *str) {
    rt_enter_critical();

    while (*str != '\0') {
        if (*str == '\n') {
            usart_send_char(DEBUG_USART, '\r');
		} 

        usart_send_char(DEBUG_USART, *str++);
	}	

    rt_exit_critical();
}

///重定向c库函数printf到串口，重定向后可使用printf函数
int fputc(int ch, FILE *f) {
    USART_SendData(DEBUG_USART, (uint8_t) ch);
    
    while (USART_GetFlagStatus(DEBUG_USART, USART_FLAG_TXE) == RESET);  // 等待发送完毕

    return (ch);
}

///重定向c库函数scanf到串口，重写向后可使用scanf、getchar等函数
int fgetc(FILE *f) {
    while (USART_GetFlagStatus(DEBUG_USART, USART_FLAG_RXNE) == RESET); // 等待串口输入数据

    return (int)USART_ReceiveData(DEBUG_USART);
}

void USART1_IRQHandler() {
    uint8_t rch;
    
    rt_interrupt_enter();
    
	if(USART_GetITStatus(usart_dev1.name, USART_IT_RXNE) != RESET) {		
		rch = USART_ReceiveData(usart_dev1.name);
#ifdef RT_USING_CONSOLE
        console_receive_buf[console_receive_count++] = rch;
        console_receive_count %= 10;
        rtt_console.rx_indicate(&rtt_console, 1);
#else
        USART_SendData(usart_dev1.name, rch);
#endif
	}
    
    rt_interrupt_leave();
}

void USART2_IRQHandler() {
    uint8_t rch;
    
    rt_interrupt_enter();
    
	if(USART_GetITStatus(usart_dev2.name, USART_IT_RXNE) != RESET) {		
		rch = USART_ReceiveData(usart_dev2.name);
        USART_SendData(usart_dev2.name, rch);
	}
    
    rt_interrupt_leave();
}

void USART3_IRQHandler() {
    uint8_t rch;
    
    rt_interrupt_enter();
    
	if(USART_GetITStatus(usart_dev3.name, USART_IT_RXNE) != RESET) {		
		rch = USART_ReceiveData(usart_dev3.name);
        USART_SendData(usart_dev3.name, rch);
	}
    
    rt_interrupt_leave();
}
