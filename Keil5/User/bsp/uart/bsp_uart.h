#ifndef __USART_H
#define	__USART_H

#include "stm32f4xx.h"
#include "common.h"
#include <stdio.h>


#define DEBUG_USART     USART1

typedef struct {
    USART_TypeDef  *name;
    uint32_t        clk;
    uint32_t        baudrate;
    gpio_pin_t      rx_pin;
    uint8_t         rx_af;
    uint8_t         rx_source;
    gpio_pin_t      tx_pin;
    uint8_t         tx_af;
    uint8_t         tx_source;
    IRQn_Type       irq;
} usart_dev_t;

/************************************************************/

void usart_init(usart_dev_t *usart);
void usart_send_char(USART_TypeDef *usartx, uint8_t ch);
void usart_send_chars(USART_TypeDef *usartx, uint8_t *str, uint32_t strlen);
void usart_send_string(USART_TypeDef *usartx, char *str);

void rt_hw_console_output(const char *str);

#ifdef RT_USING_CONSOLE
rt_size_t rtt_console_write(rt_device_t dev, rt_off_t pos, const void *buffer, rt_size_t size);
rt_size_t rtt_console_read(rt_device_t dev, rt_off_t pos, void *buffer, rt_size_t size);
#endif

#endif /* __USART1_H */
