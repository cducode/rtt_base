/**
  ******************************************************************************
  * @file    bsp_led.c
  * @author  fire
  * @version V1.0
  * @date    2015-xx-xx
  * @brief   led应用函数接口
  ******************************************************************************
  * @attention
  *
  * 实验平台:野火  STM32 F429 开发板  
  * 论坛    :http://www.firebbs.cn
  * 淘宝    :https://fire-stm32.taobao.com
  *
  ******************************************************************************
  */


#include "bsp_led.h"
#include "rtthread.h"

struct rt_thread led_thread;

rt_uint8_t led_thread_stack[512];

const gpio_pin_t led = GPIO_PIN(D, 12);

void led_init(void) {		
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_AHB1PeriphClockCmd(led.gpio_clk, ENABLE);   /*开启LED相关的GPIO外设时钟*/													   
    GPIO_InitStructure.GPIO_Pin = led.gpio_pin;	    /*选择要控制的GPIO引脚*/	
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;   /*设置引脚模式为输出模式*/
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;  /*设置引脚的输出类型为推挽输出*/
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;    /*设置引脚为上拉模式*/
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;    /*设置引脚速率为2MHz */ 
    GPIO_Init(led.gpio_port, &GPIO_InitStructure);  /*调用库函数，使用上面配置的GPIO_InitStructure初始化GPIO*/
    
    led_off();
}

void led_on(void) {
    GPIO_ResetBits(led.gpio_port, led.gpio_pin);
}

void led_off(void) {
    GPIO_SetBits(led.gpio_port, led.gpio_pin);
}

void led_thread_entry(void* parameter) {
    while(1) {
        led_on();
        rt_thread_delay(200);
        led_off();
        rt_thread_delay(200);
        led_on();
        rt_thread_delay(1000);
        led_off();
        rt_thread_delay(1000);
    }
}

int led_thread_init(void) {
    rt_thread_init(&led_thread, "led_thread", led_thread_entry, RT_NULL, &led_thread_stack, sizeof(led_thread_stack), 2, 10);
    
    return rt_thread_startup(&led_thread);
}
INIT_APP_EXPORT(led_thread_init);

/*********************************************END OF FILE**********************/
