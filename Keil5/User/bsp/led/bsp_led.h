#ifndef __LED_H
#define	__LED_H

#include "stm32f4xx.h"
#include "common.h"

void led_init(void);
void led_on(void);
void led_off(void);

void led_thread_entry(void* parameter);

#endif /* __LED_H */
