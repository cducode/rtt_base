
/*
 * Auto generated Run-Time-Environment Component Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'BH-F429' 
 * Target:  'rtt_base' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H


/*
 * Define the Device Header File: 
 */
#define CMSIS_device_header "stm32f4xx.h"

#define RTE_USING_DEVICE
#define RTE_USING_FINSH

#endif /* RTE_COMPONENTS_H */
