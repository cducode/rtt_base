# rtt_base

#### 介绍
- 开发环境 **Keil 5**，芯片 **STM32F429**。
- 使用 [RT-Thread](https://www.rt-thread.org/)，为学科竞赛做一些基础性工作。
- 可以在 **官网** [https://www.rt-thread.org/download/mdk/](https://www.rt-thread.org/download/mdk/) 下载最新的 **RealThread.RT-Thread.3.1.x.pack**，或在 Keil 的![pack_installer](https://images.gitee.com/uploads/images/2020/0523/154622_54e380d2_1997164.png "pack_installer.png")**Pack Installer** 中在线安装。

#### 资料
- RT-Thread文档中心：[https://www.rt-thread.org/document/site/#](https://www.rt-thread.org/document/site/#)